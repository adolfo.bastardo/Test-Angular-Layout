# DummyApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

For install dependecies, first, install Node.js (last version preferably, ensure the this come with 'npm'), second, install angular-cli with: `npm install -g @angular/cli`, and after this, then run: `npm install`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
