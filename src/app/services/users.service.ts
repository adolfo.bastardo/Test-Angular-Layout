import { Injectable } from '@angular/core';
import { Users } from '../models/users';
import { Observable } from 'rxjs/Observable';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';
import { createUrlResolverWithoutPackagePrefix } from '@angular/compiler';

@Injectable()
export class UsersService {
  users: Users[];

  constructor() {
    this.users = [
      {
        id: 1,
        fullName: 'Adolfo Bastardo',
        email: 'abastardo@liberarte.org.ve',
        city: 'San Felipe',
        rideInGroup: 'Always',
        dayOfWeek: 'Sun, Mon',
        dayOfRegistration: new Date(2015, 6, 26, 12, 50, 5).toLocaleString()
      },
      {
        id: 2,
        fullName: 'Alejandra Labrador',
        email: 'maria@blahblah.org.ve',
        city: 'Calabozo',
        rideInGroup: 'Never',
        dayOfWeek: 'Every Day',
        dayOfRegistration: new Date(2016, 8, 27, 5, 50, 5).toLocaleString()
      }
    ];
  }

  getUsers(): Observable<Users[]> {
    return Observable.from(this.users).toArray();
  }

  setUsers(user: Users) {
    this.users.push(user);
  }

  deleteUser(id) {
    const deleted = this.users.some((elem, i, arr) => {
      if (elem.id === id) {
        arr.splice(i, 1);
        return true;
      }
      return false;
    });

    console.log(deleted);
  }
}
