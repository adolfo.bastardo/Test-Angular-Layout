export class Users {
  constructor(
    public id: number,
    public fullName: string,
    public email: string,
    public city: string,
    public rideInGroup: string,
    public dayOfWeek: string,
    public dayOfRegistration: string
  ) {}
}
