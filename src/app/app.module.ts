import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersService } from './services/users.service';
import { NavbarComponent } from './navbar/navbar.component';
import { HelpPanelComponent } from './help-panel/help-panel.component';
import { UsersModule } from './users/users.module';

@NgModule({
  declarations: [AppComponent, NavbarComponent, HelpPanelComponent],
  imports: [BrowserModule, UsersModule],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule {}
