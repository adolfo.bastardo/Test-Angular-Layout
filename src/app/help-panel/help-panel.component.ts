import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';

@Component({
  selector: 'app-help-panel',
  templateUrl: './help-panel.component.html',
  styleUrls: ['./help-panel.component.scss']
})
export class HelpPanelComponent implements OnInit {
  opened = false;
  @Input() title: string;

  @ViewChild('panelBody') panelBody;

  constructor() {}

  ngOnInit() {}

  toggle() {
    this.opened = this.opened === true ? false : true;
  }
}
