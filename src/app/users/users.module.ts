import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UsersFormComponent } from './users-form/users-form.component';
import { UsersListComponent } from './users-list/users-list.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [UsersFormComponent, UsersListComponent],
  exports: [UsersFormComponent, UsersListComponent]
})
export class UsersModule {}
