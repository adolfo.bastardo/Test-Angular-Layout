import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Users } from '../../models/users';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss']
})
export class UsersFormComponent implements OnInit {
  radio = 'Sometimes';
  checkbox = [true, false, false, false, false, false, false];
  checkboxValues = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  user: Users;
  @ViewChild('createUserForm') form;

  constructor(private usersService: UsersService) {}

  ngOnInit() {}

  createUser(user) {
    let days = '';

    if (
      user['dayOfWeek-1'] &&
      user['dayOfWeek-2'] &&
      user['dayOfWeek-3'] &&
      user['dayOfWeek-4'] &&
      user['dayOfWeek-5'] &&
      user['dayOfWeek-6'] &&
      user['dayOfWeek-7']
    ) {
      days = 'Every day';
    } else {
      for (let i = 0; i < 7; i++) {
        if (user[`dayOfWeek-${i + 1}`]) {
          days = this.addStringWithSeparator(days, this.checkboxValues[i]);
        }
      }
      if (user['dayOfWeek-1'] && user['dayOfWeek-7']) {
        days = 'Weekends';
      } else if (
        user['dayOfWeek-2'] &&
        user['dayOfWeek-3'] &&
        user['dayOfWeek-4'] &&
        user['dayOfWeek-5'] &&
        user['dayOfWeek-6']
      ) {
        days = 'Week days';
      } else if (
        !user['dayOfWeek-1'] &&
        !user['dayOfWeek-2'] &&
        !user['dayOfWeek-3'] &&
        !user['dayOfWeek-4'] &&
        !user['dayOfWeek-5'] &&
        !user['dayOfWeek-6'] &&
        !user['dayOfWeek-7']
      ) {
        days = 'Sun';
      }
    }

    const id = this.getLastItemId() + 1;

    const ride = user['rideInGroup'] ? user['rideInGroup'] : this.radio;

    const UserReg = new Users(
      id,
      user['fullName'],
      user['email'],
      user['city'],
      ride,
      days,
      new Date().toLocaleString()
    );

    this.usersService.setUsers(UserReg);

    this.form.reset();
  }

  getLastItemId(): number {
    let id: number;
    this.usersService.getUsers().subscribe(elems => {
      id = elems[elems.length - 1].id;
    });

    return id;
  }

  addStringWithSeparator(strSrc: string, strAdd: string): string {
    let newStr = strSrc;
    if (strSrc.length > 0) {
      newStr += ', ';
    }

    newStr += strAdd;
    return newStr;
  }
}
