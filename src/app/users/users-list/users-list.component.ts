import {
  Component,
  OnInit,
  AfterViewChecked,
  AfterContentChecked
} from '@angular/core';
import { Users } from '../../models/users';
import { UsersService } from '../../services/users.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, AfterContentChecked {
  users: Users[];
  usersSubject: Subject<Users[]> = new Subject<Users[]>();

  constructor(private usersService: UsersService) {}

  ngOnInit() {
    this.usersSubject.subscribe(el => (this.users = el));
  }

  ngAfterContentChecked() {
    this.getItems();
  }

  getItems() {
    this.usersService.getUsers().subscribe(elems => {
      this.usersSubject.next(elems);
    });
  }

  deleteItem(id) {
    this.usersService.deleteUser(id);
  }
}
