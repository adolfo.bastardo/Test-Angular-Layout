import {
  Component,
  OnInit,
  Renderer2,
  ViewChild,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  stateMenu = false;
  @ViewChild('offCanvasMenu') offCanvasMenu: ElementRef;
  @ViewChild('offCanvasMenuButton') offCanvasMenuButton: ElementRef;

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {}

  openMenu(event: MouseEvent) {
    event.preventDefault();
    if (this.stateMenu === false) {
      this.renderer.addClass(document.body, 'off');
      this.renderer.addClass(this.offCanvasMenu.nativeElement, 'open');
      this.renderer.addClass(this.offCanvasMenuButton.nativeElement, 'open');
      this.stateMenu = true;
    } else {
      this.renderer.removeClass(document.body, 'off');
      this.renderer.removeClass(this.offCanvasMenu.nativeElement, 'open');
      this.renderer.removeClass(this.offCanvasMenuButton.nativeElement, 'open');
      this.stateMenu = false;
    }
  }
}
