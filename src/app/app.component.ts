import { Component, HostListener, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  closeSidebar() {
    const offButton = document.getElementById('offButton');
    if (document.body.className === 'off') {
      const ev = new MouseEvent('click');

      offButton.dispatchEvent(ev);
    }
  }
}
